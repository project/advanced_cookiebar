/**
 * @file JS file for the cookiebar module
 *
 * @author advancedTM <developers@advanced-tm.nl>
 * @author Tom Grootjans <tom@advanced-tm.nl>
 */

/* Set default variables */

(function ($, Drupal) {
  Drupal.behaviors.advancedCookiebar = {
    attach: function (context, drupalSettings) {

      $("[data-cookieconsent]").each(function () {
        var consentRequired = "primary_cookies";

        if ("advanced" === drupalSettings.advancedCookiebarConfig.cookiebarType) {
          consentRequired = $(this, context).data("cookieconsent");
        }

        if (!Drupal.advancedCookiebar.getCookieValue(consentRequired) || Drupal.advancedCookiebar.isRevisionOutdated()) {
          Drupal.advancedCookiebar.showCookieBanner($(this), drupalSettings.advancedCookiebarConfig.cookieWarning);
        }
        $(this).show();
      });

      $(".advanced-cookiebar-basic-button").click(function (e) {
        e.preventDefault();
        const primary_cookies = $(this).data("primarycookies");
        Drupal.advancedCookiebar.setCookie(primary_cookies);
        location.reload();
      });
      $('.consent__form__trigger', context).click(function () {
        $(this).toggleClass('minus');
        $(this).next().toggleClass('open');
      });

      if ((Drupal.advancedCookiebar.getCookieValue('version') === false
        || Drupal.advancedCookiebar.isRevisionOutdated())
        || Drupal.advancedCookiebar.getCookieValue('primary_cookies') === false
        || drupalSettings.advancedCookiebarConfig.currentUrl === drupalSettings.advancedCookiebarConfig.cookiepagePath) {
        Drupal.advancedCookiebar.showCookiebar();
      }

      // On form submit, save selected value as cookie
      $('form#advanced-cookiebar-form', context).on('submit', function (e) {
        Drupal.advancedCookiebar.setCookie();
        // e.preventDefault();  //prevent form from submitting
      });

      // Redirect or reload the page

    }
  };
  Drupal.advancedCookiebar = {};

  Drupal.advancedCookiebar.showCookiebar = function () {
    $("form#advanced-cookiebar-form input:checkbox").each(function () {
      if("primary_cookies" == $(this).prop('name')){
        $(this).prop("checked", true);
      } else{
        $(this).prop("checked", Drupal.advancedCookiebar.getCookieValue($(this).prop('name')));
      }
    });
    $('#advanced-cookiebar-container').show();
    return false;
  }
  Drupal.advancedCookiebar.hideCookiebar = function () {
    $('#advanced-cookiebar-container').hide();
  }

  Drupal.advancedCookiebar.isRevisionOutdated = function () {
    if (drupalSettings.advancedCookiebarConfig.checkCookieRevision === 'yes' &&
      (Drupal.advancedCookiebar.getCookieValue('version') !== drupalSettings.advancedCookiebarConfig.compliancyVersion)) {
      return true;
    }
    return false;

  }

  Drupal.advancedCookiebar.setCookie = function (primary_cookies_override) {
    var cookie = {};
    var path = drupalSettings.path.baseUrl;
    if (path.length > 1) {
      var pathEnd = path.length - 1;
      if (path.lastIndexOf('/') === pathEnd) {
        path = path.substring(0, pathEnd);
      }
    }

    cookie.primary_cookies = (primary_cookies_override !== 'undefined') ? primary_cookies_override : true; // set default for primary cookies
    cookie.version = drupalSettings.advancedCookiebarConfig.compliancyVersion;

    $("form#advanced-cookiebar-form input:checkbox").each(function () {
      var value = $(this).is(":checked");
      var id = $(this).prop('name');

      cookie[id] = value;

    });

    var expirationDate = new Date(new Date().setFullYear(new Date().getFullYear() + 1));
    $.cookie("cookieConsent", JSON.stringify(cookie), {
      expires: expirationDate,
      path: path
    });
    // location.reload();
  }
  /**
   * Get a settings from the cookie
   *
   * @param {string} name the name of the setting to get from the cookie defined in this file
   */
  Drupal.advancedCookiebar.getCookieValue = function (name) {
    var cookie = Drupal.advancedCookiebar.getCookie();
    try {
      var object = JSON.parse(cookie);
    } catch (e) {
      return false;
    }
    return object[name];
  }

  /**
   * Get the base64 encoded cookie set by this module
   */
  Drupal.advancedCookiebar.getCookie = function () {
    try {
      return $.cookie("cookieConsent");
    } catch (e) {
      return false;
    }
  }

  Drupal.advancedCookiebar.showCookieBanner = function (dom, warningHTML) {
    dom.html(warningHTML);
  }
})(jQuery, Drupal);
