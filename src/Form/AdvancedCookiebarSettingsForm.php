<?php

namespace Drupal\advanced_cookiebar\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

/**
 *
 */
class AdvancedCookiebarSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'advanced_cookiebar_settings_form';
  }

  /**
   * This method will create the settings form.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);

    // Get config.
    $config = $this->config('advanced_cookiebar.settings');
    $cookiebar_path = $config->get('advanced_cookiebar.cookiebar_path');
    $node = Node::load(($cookiebar_path) ? $cookiebar_path : 0);

    // Build form.
    $form['cookiebar_type'] = [
      '#type' => 'radios',
      '#title' => t('Cookiebar type:'),
      '#options' => [
        'basic' => t('<b>Default:</b> yes/no cookiebar'),
        'advanced' => t('<b>Advanced:</b> multiple options cookiebar block'),
      ],
      '#default_value' => $config->get('advanced_cookiebar.cookiebar_type'),
      '#description' => t('When changing this, all users will have to re-set their cookie preference.'),
    ];

    $form['cookiebar_path'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'node',
      '#title' => t('Cookies settings page'),
      '#default_value' => $node,
      '#description' => t('Page that will be used to always show the cookiebar on, this node will also be used to determine the revision ID'),
      '#required' => TRUE,
    ];

    $form['cookiebar_save_revision'] = [
      '#type' => 'radios',
      '#title' => t('Cookiebar check revision ID:'),
      '#options' => [
        "yes" => "yes",
        "no" => "no",
      ],
      '#default_value' => $config->get('advanced_cookiebar.cookiebar_save_revision'),
      '#description' => t('Select wether or not the revision ID of the Cookies settings page should be checked to see if the cookie preferences are outdated.'),
    ];

    $form['cookiebar_warning'] = [
      '#type' => 'text_format',
      '#title' => t('Cookies not set text'),
      '#format' => 'full_html',
      '#default_value' => $config->get('advanced_cookiebar.cookiebar_warning')["value"],
      '#description' => t('Default text that will be shown when cookies specified cookies aren\'t set'),
    ];
    return $form;
  }

  /**
   * Save to form by submitting it.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return parentsubmitForm
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('advanced_cookiebar.settings');
    $config->set('advanced_cookiebar.cookiebar_type', $form_state->getValue('cookiebar_type'));
    $config->set('advanced_cookiebar.cookiebar_path', $form_state->getValue('cookiebar_path'));
    $config->set('advanced_cookiebar.cookiebar_save_revision', $form_state->getValue('cookiebar_save_revision'));
    $config->set('advanced_cookiebar.cookiebar_warning', $form_state->getValue('cookiebar_warning'));
    $config->save();

    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'advanced_cookiebar.settings',
    ];
  }

}
