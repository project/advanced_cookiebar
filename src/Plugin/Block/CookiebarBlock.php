<?php

namespace Drupal\advanced_cookiebar\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a cookiebar.
 *
 * @Block(
 *   id = "advanced_cookiebar_block",
 *   admin_label = "Advanced Cookiebar",
 *   category = "Cookiebar",
 * )
 */
class CookiebarBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Get Cookiebar settings.
    $config = \Drupal::config('advanced_cookiebar.settings');
    return [
      '#theme' => ($config->get('advanced_cookiebar.cookiebar_type') === "advanced") ? "advanced_cookiebar_advanced" : "advanced_cookiebar_basic",
      '#label' => [
        '#markup' => $config->get('advanced_cookiebar.label'),
      ],
      '#text' => [
        '#markup' => $config->get('advanced_cookiebar.text'),
      ],
      '#accept_button_text' => [
        '#markup' => $config->get('advanced_cookiebar.accept_button_text'),
      ],
      '#decline_button_text' => [
        '#markup' => $config->get('advanced_cookiebar.decline_button_text'),
      ],
      '#cookies' => $config->get('advanced_cookiebar.settings_cookies'),
      '#attached' => [
        'library' => [
          "advanced_cookiebar/advanced-cookiebar-js",
        ],
      ],
    ];
  }

}
